# Setting up tailwind using node

## installing tailwind

it will install tailwind node module and create tailwind.config.js file.

```
npm install -D tailwindcss
npx tailwindcss init
```

## update tailwind.config.js


```
module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {},
  },
  plugins: [],
}
```

## Adding tailwind directives to our css 

```
@tailwind base;
@tailwind components;
@tailwind utilities;
```

## Building output css file

```
npx tailwindcss -i ./src/input.css -o ./dist/output.css --watch
```

## Add css file in HTML page

```
  <link href="/dist/output.css" rel="stylesheet">
```
